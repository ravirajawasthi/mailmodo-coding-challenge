//Components
import PackageTable from "../PackageTable";
import PackageTrackingStatus from "../PackageTrackingStatus";

//CSS File
import "./PackageInformation.css";

const PackageInformation = () => {
  //Wraps PackageTrackingStatus and PrackageTable
  return (
    <div className="PackageInformation-root w-full flex items-center justify-between">
      <PackageTrackingStatus />
      <PackageTable />
    </div>
  );
};

export default PackageInformation;
