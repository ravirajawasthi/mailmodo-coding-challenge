//Context
import { useContext } from "react";
import {
  DispatchContext,
  PackagesContext,
} from "../../contexts/packages.context";

//Components and types
import { PackageEntityParameter } from "../../interfaces/types";
import PackageTableRow from "../PackageTableRow";

//util
import { loading_markup } from "./util";

//Assets
import down from "../../assets/down_blue.svg";
import uparrow from "../../assets/uparrow.svg";

//CSS
import "./PackageTable.css";

const PackageTable = () => {
  const {
    packages_array,
    sort_parameter,
    assending,
    raw_packages_array,
  } = useContext(PackagesContext); //getting package context, refer to its type
  const dispatch = useContext(DispatchContext); //dispatch function, to change state

  const highlightSelectedPackage = (id: string, awbno: string) => {
    //on click change selected package for tracking
    dispatch({ type: "SELECT_PACKAGE", id, awbno });
  };

  const triggerSort = (parameter: PackageEntityParameter) => {
    //On click over header, sort depending on previous config
    dispatch({
      type: "SORT",
      parameter,
      assending: sort_parameter === parameter ? !assending : true,
    });
  };

  //Class string, for headers
  const header_item_class_string = "column-width text-center text-xs";

  //descending arrow markup, used beside table header
  const descending_arrow_markup = (
    <img
      src={down}
      alt="descending"
      className=" h-3 ml-2 inline-block text-blue-600"
    />
  );

  //assending arrow markup, used beside table header
  const assending_arrow_markup = (
    <img
      src={uparrow}
      alt="descending"
      className=" h-3 ml-2 inline-block text-blue-600"
    />
  );

  return (
    <div className=" h-full w-4/6 inline-block ">
      <div className="PackageTable-root w-full m-auto border border-blue-200 overflow-y-scroll pl-4">
        {/*Table is not constructed using table, hence it is constructed using divs*/}
        <header className="flex items-center w-full py-4 text-gray-400 uppercase sticky top-0 bg-white">
          {/*awbno. column*/}

          <div
            key="awbno"
            className={`${header_item_class_string} cursor-pointer`}
            onClick={() => triggerSort("awbno")}
          >
            {/*binded function to trigger sort on click*/}
            AWB Number
            {sort_parameter === "awbno"
              ? assending
                ? assending_arrow_markup
                : descending_arrow_markup
              : null}
            {/*Add sort arrow condition. if sort_parameter matches with the type of type of table column head*/}
          </div>

          {/*Transporter. column*/}

          <div
            className={`${header_item_class_string} cursor-pointer`}
            onClick={() => triggerSort("carrier")}
          >
            Transporter
            {sort_parameter === "carrier"
              ? assending
                ? assending_arrow_markup
                : descending_arrow_markup
              : null}
          </div>

          {/*source. column*/}

          <div key="source" className={header_item_class_string}>
            Source
          </div>

          {/*destination. column*/}

          <div key="destination" className={header_item_class_string}>
            Destination
          </div>

          {/*package createdAt column*/}

          <div
            key="createdAt"
            className={`${header_item_class_string} cursor-pointer`}
            onClick={() => triggerSort("createdAt")}
          >
            Start Date
            {sort_parameter === "createdAt"
              ? assending
                ? assending_arrow_markup
                : descending_arrow_markup
              : null}
          </div>

          {/*estimated delivery date*/}

          <div key="ETD" className={header_item_class_string}>
            ETD
          </div>

          {/*current status code column*/}

          <div
            key="current_status_code"
            className={`${header_item_class_string} cursor-pointer`}
            onClick={() => triggerSort("current_status_code")}
          >
            Status
            {sort_parameter === "current_status_code"
              ? assending
                ? assending_arrow_markup
                : descending_arrow_markup
              : null}
          </div>
        </header>
        {/*Rendering all rows of table, depending on filter*/}
        {/*loading_markup*/}
        {packages_array.length === 0 &&
          raw_packages_array.length === 0 &&
          loading_markup}{" "}
        {/*Package table row*/}
        {packages_array.map((pkg) => (
          <PackageTableRow
            key={pkg._id}
            pkg={pkg}
            handleClick={() => highlightSelectedPackage(pkg._id, pkg.awbno)}
          />
        ))}
        {/*loading_markup*/}
      </div>
    </div>
  );
};

export default PackageTable;
