export const loading_markup = (
  //Animating pulse effect, 6 bars
  <div className="h-4/5 w-full m-auto">
    {new Array(6).map((_) => (
      <div className="animate-pulse flex  bg-gray-200 h-1/6 w-full my-1" />
    ))}
  </div>
);
