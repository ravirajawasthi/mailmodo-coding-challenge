import { PackageEnity, PackageFilter } from "../../interfaces/types";

export const FILTER_ORDER: PackageFilter[] = [
  "DEL",
  "INT",
  "OOD",
  "DEX",
  "NFI",
  "UND",
];

export const updateFilterCountOnPackageChange = (
  raw_package_list: PackageEnity[]
) => {
  //Runs only on mount, because number of packages don't change
  const freq = {
    DEL: 0,
    INT: 0,
    OOD: 0,
    DEX: 0,
    NFI: 0,
    UND: 0,
  };
  raw_package_list.forEach((pkg) => {
    freq[pkg.current_status_code] += 1;
  });

  return freq;
};
