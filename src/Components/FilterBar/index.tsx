//Context and react stuff
import { useContext, useEffect, useState } from "react";
import {
  DispatchContext,
  PackagesContext,
} from "../../contexts/packages.context";

//Types and Components
import { PackageFilter } from "../../interfaces/types";
import FilterButton from "../FilterButton";

//util function
import { FILTER_ORDER, updateFilterCountOnPackageChange } from "./util";

const FilterBar = () => {
  //Context
  const Packages = useContext(PackagesContext);
  const dispatch = useContext(DispatchContext);

  const [filter, setFilter] = useState<PackageFilter[]>(["DEL"]); //Array contains code of filter whose package is to be displayed
  const [count, setCount] = useState({
    DEL: 0,
    INT: 0,
    OOD: 0,
    DEX: 0,
    NFI: 0,
    UND: 0,
  }); //Count of all types of packages, only calculated on mount

  useEffect(() => {
    //Count packages only on mount
    setCount(updateFilterCountOnPackageChange(Packages.raw_packages_array));
  }, [Packages.raw_packages_array]);

  const handleOnClick = (type: PackageFilter, active: boolean) => {
    //Dispatch event depending if currnet filter is active
    if (!active) {
      const newFilterArray = [...filter, type];
      dispatch({ type: "CHANGE_FILTER", filter: newFilterArray });
      setFilter(newFilterArray);
    } else {
      const newFilterArray = filter.filter((item) => item !== type);
      dispatch({
        type: "CHANGE_FILTER",
        filter: newFilterArray,
      });
      setFilter(newFilterArray);
    }
  };

  return (
    <div className="h-1/4 min-w-full flex flex-row items-center justify-center">
      {FILTER_ORDER.map((item) => (
        <FilterButton
          key={item}
          onClickFunction={() => handleOnClick(item, filter.includes(item))}
          type={item}
          count={count[item]}
          active={filter.includes(item)}
        />
      ))}
    </div>
  );
};

export default FilterBar;
