//Assets
import logo from "../../assets/logo.svg";
import profile from "../../assets/profile.svg";
import down from "../../assets/down.svg";

//CSS File
import "./Navbar.css";

function Navbar() {
  //Navbar, html markup
  return (
    <div className="Navbar-root min-w-full bg-gray-50 flex flex-row justify-between items-center px-8 shadow-md">
      <div className="flex w-1/5 items-center">
        <img src={logo} alt="logo" />
        <button className="font-bold text-lg">Intugine</button>
      </div>
      <div className="flex justify-evenly items-center h-full">
        <button className="font-bold h-full px-5 hover:bg-blue-50 transition duration-200 ease-in-out">
          Home
        </button>
        <button className="font-bold h-full px-5 hover:bg-blue-50 transition duration-200 ease-in-out">
          Brands
        </button>
        <button className="font-bold h-full px-5 hover:bg-blue-50 transition duration-200 ease-in-out">
          Transporters
        </button>
        <button className="p-3 mx-3 rounded-full bg-gray-300 flex items-center">
          <img src={profile} alt="profile" />
        </button>
        <button className="px-5  flex items-center">
          <img src={down} alt="options" className="h-5" />
        </button>
      </div>
    </div>
  );
}

export default Navbar;
