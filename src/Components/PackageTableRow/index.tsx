//Context
import { FC, ReactElement, useContext } from "react";
import { PackagesContext } from "../../contexts/packages.context";
import { PackageEnity } from "../../interfaces/types";

//util fucntions
import { parseDate } from "./util";
import clsx from "clsx";

//Row item class string, to prevent repetition.
const row_item_class_string = "column-width text-center text-xs";

const PackageTableRow: FC<PackageTableRowProps> = ({
  pkg,
  handleClick,
}): ReactElement => {
  const { selected_package } = useContext(PackagesContext);

  return (
    <div
      className={
        "flex items-center w-full uppercase my-5 py-5 border border-gray-100 text-gray-600 hover:bg-blue-50 transition  ease-in-out text" +
        clsx("bg-gary-100" && selected_package?._id === pkg._id)
      }
      onClick={handleClick}
    >
      {/* handleClick passed down from parent, used to highlight package for tracking */}
      <div className={row_item_class_string}>{`#${pkg.awbno}`}</div>
      <div className={row_item_class_string}>{pkg.carrier}</div>
      <div className={row_item_class_string}>{pkg.from}</div>
      <div className={row_item_class_string}>{pkg.to}</div>
      <div className={row_item_class_string}>{parseDate(pkg.createdAt)}</div>
      <div className={row_item_class_string}>
        {parseDate(pkg.extra_fields?.expected_delivery_date)}
      </div>
      <div
        className={`${row_item_class_string} ${
          codeToStyleString[pkg.current_status_code]
        }`}
      >
        {codeToText[pkg.current_status_code]}
      </div>
    </div>
  );
};

type PackageTableRowProps = {
  pkg: PackageEnity;
  handleClick: () => void;
}; //Props type

export default PackageTableRow;

//Get text color depending on package status code
export const codeToStyleString = {
  DEL: "text-green-500",
  INT: "text-blue-600",
  OOD: "text-yellow-500",
  NFI: "text-red-800",
  UND: "text-red-800",
  DEX: "",
};

//Get display message depending on package status code
export const codeToText = {
  DEL: "Delivered",
  INT: "In-Transit",
  OOD: "Out for Delivery",
  NFI: "No Information Yet",
  UND: "Un-delivered",
  DEX: "--",
};
