export const parseDate = (date: string | undefined) => {
  //Parses date and returns a string representation
  if (date === undefined) return "--";
  const d = new Date(date);
  return `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
};
