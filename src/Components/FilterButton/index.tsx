import React from "react";

import clsx from "clsx"; //Used for conditionally applying classed

//Types
import { FC, ReactElement } from "react";
import { PackageFilter } from "../../interfaces/types";

const FilterButton: FC<FilterButtonProps> = ({
  type,
  count,
  active,
  onClickFunction,
}): ReactElement => {
  return (
    <button
      onClick={onClickFunction}
      className={
        "w-32 mx-3 h-32 rounded-md relative " +
        clsx(
          active && "bg-blue-700 text-white",
          !active && "bg-indigo-200 text-blue-700"
        )
      }
    >
      {/* onClickFunction passed down from parent */}
      <div className="font-bold text-md absolute top-2 left-4">{type}</div>{" "}
      {/* name of filter */}
      <div className="text-5xl absolute bottom-2 text-center w-full font-light">
        {count}
      </div>
      {/* count displayed */}
    </button>
  );
};

type FilterButtonProps = {
  type: PackageFilter;
  count: Number;
  active: Boolean;
  onClickFunction: () => void;
};

export default React.memo(FilterButton);
