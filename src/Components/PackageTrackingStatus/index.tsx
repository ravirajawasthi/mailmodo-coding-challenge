//Context
import { useContext } from "react";
import { PackagesContext } from "../../contexts/packages.context";

//util
import { genTrackingMarkup } from "./util";

const PackageTrackingStatus = () => {
  //PackageContext
  const { selected_package } = useContext(PackagesContext);

  //Markup depending on selected_package
  const no_package_selected_markup = (
    <div className="text-gray-200 text-center flex justify-center items-center py-auto uppercase text-4xl h-full">
      <h2>Select a package</h2>
    </div>
  );

  const no_information_markup = (
    <div className="text-red-600 text-center flex justify-center items-center py-auto uppercase text-4xl h-full">
      <h2>No Information yet</h2>
    </div>
  );

  return (
    <div className="w-2/6 h-4/5">
      <div className="w-4/5 h-5/6 border border-blue-300 mx-auto">
        {/* no package selected */}
        {selected_package === undefined && no_package_selected_markup}
        {/* NFI Package */}
        {selected_package?.current_status_code === "NFI" &&
          selected_package !== undefined &&
          no_information_markup}
        {/* Normal package */}
        {selected_package?.current_status_code !== "NFI" &&
          selected_package !== undefined &&
          genTrackingMarkup(selected_package)}
      </div>
    </div>
  );
};

export default PackageTrackingStatus;
