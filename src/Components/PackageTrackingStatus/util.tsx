//util functions
import { codeToStyleString } from "../PackageTableRow";
import { parseDate } from "../PackageTableRow/util";

//Assets
import source from "../../assets/warehouse.svg";
import destination from "../../assets/destination.svg";

//Types
import { PackageEnity } from "../../interfaces/types";
import { generateRandomString } from "../../contexts/util";
import clsx from "clsx";

export const genTrackingMarkup = (pkg: PackageEnity) => {
  let scan = pkg.scan!;

  return (
    <div className="h-full w-full relative">
      {/* svg images with timeline between source and destination */}
      <div className="h-full w-1/5 relative inline-block">
        <div className="top-5 left-5 absolute bg-indigo-300 p-5 rounded-full z-10">
          <img src={destination} alt="destination" />
        </div>
        <div className="bottom-5 left-5 absolute bg-indigo-300 p-5 rounded-full z-10">
          <img src={source} alt="source" />
        </div>
        <div className="h-full w-1/5 relative border-r-2 border-dashed border-blue-600 left-7"></div>
      </div>

      {/* package scans parent element */}

      <div className="h-full w-4/5  inline-block absolute m-auto">
        <div
          className="h-full w-full
          flex justify-center items-center "
        >
          {/* rendering a div for every scan element */}

          <div className="w-full h-4/5 overflow-y-auto">
            {/* rendering  scan elements*/}

            {scan.map((scan, i) => {
              return (
                <div
                  className={
                    "text-xs border border-gray-300 p-2 m-2  transition ease-in-out " +
                    clsx(
                      i !== 0 && "hover:bg-blue-50",
                      i === 0 &&
                        `bg-blue-50 ${
                          codeToStyleString[pkg.current_status_code]
                        }`
                    ) //Adding code and bg to first scan
                  }
                  key={generateRandomString()}
                >{`${scan.location} ${parseDate(scan.time)}`}</div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
