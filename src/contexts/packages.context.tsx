//Context and react
import {
  createContext,
  useReducer,
  FC,
  ReactElement,
  Dispatch,
  useEffect,
} from "react";
import packageReducer from "../reducers/package.reducer";

//Types
import { PackageActionType } from "../interfaces/types";

//UTIL
import { getAPIData, constructDefaultPackageEntity } from "./util";

const defaultDispatch: Dispatch<PackageActionType> = () => null; //Default dispatch function

const PackagesContext = createContext(constructDefaultPackageEntity());
const DispatchContext = createContext(defaultDispatch);

const PackageContextWrapper: FC = (props): ReactElement => {
  //Reducer
  const [packages, dispatch] = useReducer(
    packageReducer,
    constructDefaultPackageEntity()
  );

  //getAPIData is called only on mount
  useEffect(() => {
    getAPIData().then((populatedPackageEntity) => {
      dispatch({ type: "SET_PACKAGE_OBJECT", package: populatedPackageEntity });
    });
  }, []);

  return (
    <PackagesContext.Provider value={packages}>
      <DispatchContext.Provider value={dispatch}>
        {/* all elements in between are rendered via props.children */}
        {props.children}
      </DispatchContext.Provider>
    </PackagesContext.Provider>
  );
};

export { PackageContextWrapper, PackagesContext, DispatchContext };
