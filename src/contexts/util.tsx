import axios from "axios";
import { PackageEnity, Packages } from "../interfaces/types";
import { constructTable } from "../reducers/util";
import { POST_URL, TOKEN, BODY, c_data } from "./constants";

export const getAPIData = async () => {
  //getting api data, if it fails falling back to localcopy of data

  //TODO: ERROR HANDLING
  let data: PackageEnity[] = [];
  try {
    const response = await axios.post(POST_URL, BODY, {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
    });
    data = response.data;
  } catch (exception) {
    console.log(`fetching data from api failed`);
    data = c_data;
  }
  //Populating missing Id's
  const APIData = data.map(
    (pkg): PackageEnity => {
      if (pkg._id.length < 1) pkg._id = generateRandomString();
      if (pkg.scan === undefined) pkg.scan = [];
      return pkg;
    }
  );
  //Processing and returning populated Package Object

  const populatedPackageEntity = constructDefaultPackageEntity();
  populatedPackageEntity.raw_packages_array = APIData;
  populatedPackageEntity.packages_array = constructTable(["DEL"], APIData, {
    sort_parameter: "awbno",
    assending: true,
  });
  populatedPackageEntity.total_packages =
    populatedPackageEntity.packages_array.length;

  return populatedPackageEntity;
};

export const constructDefaultPackageEntity = () => {
  //Default Package object
  const defaultPackages: Packages = {
    raw_packages_array: [],
    packages_array: [],
    selected_package: undefined,
    sort_parameter: "awbno",
    assending: true,
    total_packages: 0,
  };
  return defaultPackages;
};

export const generateRandomString = () =>
  Math.random().toString(36).substring(7);
