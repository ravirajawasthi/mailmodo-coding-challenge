//Components
import FilterBar from "./Components/FilterBar";
import Navbar from "./Components/Navbar";

//Context
import PackageInformation from "./Components/PackageInformation";
import { PackageContextWrapper } from "./contexts/packages.context";

function App() {
  return (
    <>
      <Navbar />
      <PackageContextWrapper>
        <FilterBar />
        <PackageInformation />
      </PackageContextWrapper>
    </>
  );
}

export default App;
