export interface Packages {
  raw_packages_array: PackageEnity[];
  packages_array: PackageEnity[];
  selected_package: PackageEnity | undefined;
  sort_parameter: PackageEntityParameter;
  assending: boolean;
  total_packages: number;
}
export type FilterMap = Map<PackageFilter, boolean>;
export type CountMap = Map<PackageFilter, number>;
export interface PackageEnity {
  _id: string;
  awbno: string;
  carrier: string;
  pickup_date?: string | null;
  current_status: string;
  current_status_code: PackageFilter;
  order_data: string;
  recipient?: string | null;
  extra_fields?: ExtraFields | null;
  from?: string | null;
  to?: string | null;
  time?: string | null;
  scan: ScanEntity[];
  createdAt: string;
  customer_name?: string | null;
}
export interface ExtraFields {
  expected_delivery_date: string;
  destination_pincode?: string | null;
  update_by?: string | null;
  quantity?: string | null;
  weight?: string | null;
}
export interface ScanEntity {
  time: string;
  location: string;
  status_detail: string;
}

export type PackageEntityParameter =
  | "awbno"
  | "carrier"
  | "current_status_code"
  | "createdAt";

export type PackageActionType =
  | { type: "SORT"; parameter: PackageEntityParameter; assending: boolean }
  | { type: "SELECT_PACKAGE"; id: string; awbno: string }
  | {
      type: "CHANGE_FILTER";
      filter: PackageFilter[];
    }
  | { type: "SET_PACKAGE_OBJECT"; package: Packages };

export type PackageFilter = "DEL" | "INT" | "OOD" | "DEX" | "NFI" | "UND";

export type PackageFilterCount = {
  type: PackageFilter;
  count: number;
};
