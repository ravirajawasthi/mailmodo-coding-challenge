import {
  PackageEnity,
  PackageEntityParameter,
  PackageFilter,
} from "../interfaces/types";

export function sortArrayOfObject(
  array: PackageEnity[],
  parameter: PackageEntityParameter,
  assending: boolean
) {
  //util function for sorting array of objects
  array.sort((a, b) => {
    if (a[parameter] < b[parameter]) {
      return assending === true ? -1 : 1;
    } else if (a[parameter] > b[parameter]) {
      return assending === true ? 1 : -1;
    } else return 0;
  });
  return array;
}

export function constructTable(
  filter: PackageFilter[],
  raw_packages: PackageEnity[],
  sort_config: { sort_parameter: PackageEntityParameter; assending: boolean }
) {
  //reconstruct table when filters change
  const filtered_packages = raw_packages.filter((pkg) =>
    filter.includes(pkg.current_status_code)
  );
  sortArrayOfObject(
    filtered_packages,
    sort_config.sort_parameter,
    sort_config.assending
  );

  return filtered_packages;
}
