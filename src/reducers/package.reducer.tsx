import { PackageActionType, Packages } from "../interfaces/types";
import { constructTable, sortArrayOfObject } from "./util";

function packageReducer(packages: Packages, action: PackageActionType) {
  switch (action.type) {
    case "SET_PACKAGE_OBJECT":
      //Setting package object after getting data from api
      return { ...action.package };

    case "SORT":
      //Sort table based on selected parameter
      const sortParameter = action.parameter;
      const assending = action.assending;
      packages.packages_array = sortArrayOfObject(
        packages.packages_array,
        sortParameter,
        assending
      );
      packages.sort_parameter = action.parameter;
      packages.assending = action.assending;
      return { ...packages };

    case "SELECT_PACKAGE":
      //For highlighting package whose tracking status has to be displayed
      const { id, awbno } = action;
      const foundpackage = packages.packages_array.find(
        (pkg) => pkg._id === id && awbno === pkg.awbno
      );
      packages.selected_package = foundpackage;
      return { ...packages };

    case "CHANGE_FILTER":
      //Reconstructing Table after changing filters
      packages.packages_array = constructTable(
        action.filter,
        packages.raw_packages_array,
        {
          sort_parameter: packages.sort_parameter,
          assending: packages.assending,
        }
      );
      packages.total_packages = packages.packages_array.length;

      return { ...packages };

    default:
      return { ...packages };
  }
}

export default packageReducer;
