import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

//Getting react to render on div with id root
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
